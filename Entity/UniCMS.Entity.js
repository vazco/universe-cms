'use strict';

/**
 * Creates new UniCMS Entity
 * @param {String} name - Name of the entity, also collection name by default
 * @param {Object=} [componentsToAdd] - List of used components
 * @param {Object} [options] - Additional options
 * @param {Boolean} [options.dynamicPubSub=true] - Automatically create publications
 * @param {Object|Boolean} [options.dynamicRouter={}] - Automatically create routes (requires iron:router)
 * You can pass config object or false to disable dynamicRouter.
 * @param {Object} [options.allow] - Set collection allow rules
 * @param {Object} [options.deny] - Set collection deny rules
 * @param {String} [options.collectionName] - Set collection name other than entity name
 * @throws {Meteor.Error}
 * @constructor
 */
UniCMS.Entity = function (name, componentsToAdd, options) {
    check(name, String);
    var entity = this;

    // Verify entity arguments

    if (!/^[a-zA-Z][0-9a-zA-Z_]*$/.test(name)) {
        throw new Meteor.Error(500, 'Entity name must start with letter and contain only letters, numbers and _');
    }

    if (!(entity instanceof UniCMS.Entity)) {
        throw new Meteor.Error(500, 'UniCMS.Entity is a constructor');
    }

    if (!name) {
        throw new Meteor.Error(500, 'UniCMS entity requires a name');
    }
    entity.name = name;
    // Add new entity to registry

    if (UniCMS._entities[name]) {
        throw new Meteor.Error(500, name + ' entity already exists');
    }
    UniCMS._entities[name] = entity;

    // Set default options

    entity.options = options = _(options || {}).defaults({
        collectionName: name,
        dynamicPubSub: true,
        dynamicRouter: {},
        docHelpers: {}
    });

    // Create collection
    var collection = entity._collection = new UniCollection(options.collectionName, {
        mixins: [new UniCollection.mixins.PublishAccessMixin()]
    });
    collection.getEntity = function () {
        return entity;
    };
    //Set UniCMS.EntityDoc
    collection.setDocumentClass(UniCMS.EntityDoc.extend(), true);
    // Set custom entity doc helpers
    if (options.docHelpers) {
        collection.docHelpers(options.docHelpers);
    }

    // Attach components to entity

    entity._components = [];

    if (componentsToAdd) {
        entity.addComponents(componentsToAdd);
    }

    // Set allow/deny rules

    if (entity.options.allow) {
        collection.allow(options.allow);
    }
    if (entity.options.deny) {
        collection.deny(options.deny);
    }

    // Create publication if requested

    if (options.dynamicPubSub && Meteor.isServer) {
        entity._initDynamicPubSub();
    }

    // Create routes if requested

    if (options.dynamicRouter && Meteor.isClient && typeof Iron !== 'undefined' && Iron.Controller) {
        entity._initDynamicRouter(options.dynamicRouter);
    }

    // Create shorthand methods for entity

    ['find', 'findOne', 'allow', 'deny', 'insert', 'update', 'remove'].forEach(function (method) {
        // CR: Changed because the bind() method creates a new function,
        // and some of packages like collection hooks replace org functions by own.

        entity[method] = function(){
            return collection[method].apply(collection, arguments);
        };
    });

    if (Meteor.isClient) {
        // Reactive dictionaries
        entity.editModes = new ReactiveDict();

        // Add autoform hooks
        entity.hooks = {
            insert: _({}).extend(defaultEntityInsertHooks),
            update: _({}).extend(defaultEntityUpdateHooks)
        };
    }

};

UniCMS.Entity.prototype.getCollection = function () {
    return this._collection;
};

/**
 * Adds new component to entity
 * @param {String} name - Name of the component
 * @param {UniCMS.Component} component - component
 * @param {Boolean} [updateSchema=true] - pass false to disable schema update
 * @returns {UniCMS.Entity} entity
 */
UniCMS.Entity.prototype.addComponent = function (name, component, updateSchema) {
    check(name, String);
    check(component, UniCMS.Component);

    if (this.getComponent(name)) {
        // if there is already component with this name, overwrite it
        console.log('UniCMS: component ' + name + ' is being overwritten in ' + this.name);
        this.removeComponent(name);
    }

    component.name = name;
    this._components.push(component);

    if (updateSchema !== false) {
        this.updateSchema();
    }
    return this;
};

/**
 * Shorthand for adding many components
 * @param {{}} components - Name: UniCMS.Component hashmap
 * @returns {UniCMS.Entity} entity
 */
UniCMS.Entity.prototype.addComponents = function (components) {
    check(components, Object);
    _(components).each(function (component, name) {
        this.addComponent(name, component, false);
    }, this);
    this.updateSchema();

    return this;
};

/**
 * Removes (in place) component from entity
 * @param {String} name - Name of the component to remove
 * @throws {Meteor.Error}
 * @returns {UniCMS.Entity} entity
 */
UniCMS.Entity.prototype.removeComponent = function (name) {
    check(name, String);
    //There is no findIndex in meteor underscore...
    var index = UniUtils.findKey(this._components, function (component) {
        return component.name === name;
    });

    if (index !== undefined) {
        this._components.splice(index, 1);
    }

    this.updateSchema();
    return this;
};

/**
 * Change order of the components on a entity.
 * You can also filter out some components if you want to remove them.
 * @param {Function} fn - Function that will take current components as a param and should return array with components in new order
 * @returns {UniCMS.Entity} entity
 */
UniCMS.Entity.prototype.reorderComponents = function (fn) {
    check(fn, Function);

    var newComponents = fn(this._components);
    check(newComponents, [UniCMS.Component]);

    this._components = newComponents;

    this.updateSchema();
    return this;
};

/**
 * Returns component by name
 * @param {String} name - component name
 * @returns {UniCMS.Component} component
 */
UniCMS.Entity.prototype.getComponent = function (name) {
    check(name, String);
    return _(this._components).findWhere({name: name});
};

/**
 * Returns sorted array of component objects.
 * @param {[String]} [whitelist] - You can pass array to narrow components by name
 * @returns {[UniCMS.Component]} array of components
 */
UniCMS.Entity.prototype.getComponents = function (whitelist) {
    if (whitelist) {
        check(whitelist, Array);
        return this._components.filter(function (component) {
            return whitelist.indexOf(component.name) !== -1;
        });
    }
    return this._components.slice(); //don't pass this as a reference
};


/**
 * Get schema from base + components and attach it to collection
 * @returns {UniCMS.Entity} entity
 */
UniCMS.Entity.prototype.updateSchema = function () {
    this._collection.setSchema(new SimpleSchema(this.getSchema()), {replace: true});
    return this;
};

/**
 * Get basic schema for entity
 * @type {Function}
 * @returns {Object} schema object
 */
UniCMS.Entity.prototype.getBasicSchema = UniCollection.getBasicSchema;

/**
 * Return schema for whole entity
 * @returns {Object} schema object
 */
UniCMS.Entity.prototype.getSchema = function () {
    var schema = {};
    this._components.forEach(function (component) {
        schema[component.name] = component.getSchema();
    });
    return _.extend(this.getBasicSchema(), schema);
};

/**
 * Returns fields in autoform-compatible form
 * @returns {string} comma delimited string
 */
UniCMS.Entity.prototype.getInsertFields = function () {
    return this._components.map(function (component) {
        return component.name;
    }).join(',');
};

/**
 * Returns fields in autoform-compatible form
 * @returns {string} comma delimited string
 */
UniCMS.Entity.prototype.getUpdateFields = function () {
    return this._components.map(function (component) {
        return component.name;
    }).join(',');
};

/**
 * Sets or gets mode for doc in entity
 * @param {String} docId id of doc
 * @param {boolean} mode boolean
 * @returns {boolean|undefined} entity mode
 */
UniCMS.Entity.prototype.editMode = function (docId, mode) {
    if (typeof mode === 'undefined') {
        return !!this.editModes.get(docId);
    }
    this.editModes.set(docId, !!mode);
    return mode;
};

if (Meteor.isClient) {
    /**
     * Generates insert form id
     * @returns {string} fomrId
     */
    UniCMS.Entity.prototype.getInsertFormId = function () {
        return this.name + 'InsertForm';
    };

    /**
     * Generates update form id
     * @param {string} id doc id
     * @returns {string} formId
     */
    UniCMS.Entity.prototype.getUpdateFormId = function (id) {
        check(id, String);
        return this.name + 'UpdateForm_' + id;
    };

    /**
     * Default autoform hooks
     */
    var defaultEntityInsertHooks = {
        onSuccess: function () {
            try {
                if(typeof Router !== 'undefined') {
                    Router.go(UniCMS.entity().name + 'View', {_id: this.docId});
                } else if (typeof FlowRouter !== 'undefined') {
                    FlowRouter.go(UniCMS.entity().name + 'View', {_id: this.docId});
                }
            } catch (e) {
                console.warn('[UniCMS] Cannot redirect to entity. ' + e);
            }
        }
    };

    var defaultEntityUpdateHooks = {
        onSuccess: function () {
            try {
                this.template.data.doc.editMode(false);
            } catch (e) {
                console.warn('[UniCMS] Cannot set edit mode to false. ' + e);
            }
        }
    };
}


