'use strict';

UniCMS.EntityDoc = UniCollection.UniDoc.extend();

UniCMS.EntityDoc.prototype.getEntity = function(){
    return this.getCollection().getEntity();
};

UniCMS.EntityDoc.prototype.getComponent = function(){
    var entity = this.getEntity();
    return entity.getComponent.apply(entity, arguments);
};

UniCMS.EntityDoc.prototype.getComponents = function(){
    var entity = this.getEntity();
    return entity.getComponents.apply(entity, arguments);
};

UniCMS.EntityDoc.prototype.editMode = function(mode){
    // Reactive dict doesn't persist on hot code reload, we may need to move it to global session.
    var entity = this.getEntity();
    if (typeof mode === 'undefined') {
        // getter
        return entity.editMode(this._id);
    }
    // setter
    mode = !!mode;
    return entity.editMode(this._id, mode);
};

UniCMS.EntityDoc.prototype.toggleEditMode = function () {
    var mode = !this.editMode();
    this.editMode(mode);
    return mode;
};