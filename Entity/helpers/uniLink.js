'use strict';

Template.uniLink.created = function () {
    try {
        check(Template.parentData().entity, UniCMS.Entity);
    } catch (e) {
        console.error('[UniCMS] uniLink can be used only inside uniEntity context. ' + e);
    }
};
