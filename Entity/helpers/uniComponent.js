'use strict';

Template.uniComponent.helpers({
    setComponent: function () {
        var doc, entity, component;
        try {
            doc = this.doc || Template.parentData().doc;
            if (doc) {
                // we got passed doc
                check(doc, UniCollection.UniDoc);
                component = doc.getComponent(this.name);
            } else {
                // without doc we need to get component directly from entity
                entity = this.entity || Template.parentData().entity || UniCMS.entity();
                check(entity, UniCMS.Entity);
                component = entity.getComponent(this.name);
            }
            check(component, UniCMS.Component);
            this.component = component;
        } catch (e) {
            console.error('[UniCMS] Cannot set component in context for ' + this.name + '. ' + e);
        }
    },
    setContentBlocks: function (contentBlock, elseBlock) {
        if (Match.test(contentBlock, Blaze.Template)) {
            this.componentContentBlock = contentBlock;
        }
        if (Match.test(elseBlock, Blaze.Template)) {
            this.componentElseBlock = elseBlock;
        }
    },
    getTemplate: function () {
        try {
            if (this.template) {
                return this.template;
            }
            check(this.component, UniCMS.Component);
            return this.component.template;
        } catch (e) {
            console.error('[UniCMS] Cannot find template for component ' + this.name + '. ' + e);
            return null;
        }
    },
    getData: function () {
        var doc;
        try {
            if (this.data) {
                return this.data;
            }
            doc = this.doc || Template.parentData().doc;
            check(doc, UniCollection.UniDoc);
            return doc[this.name];
        } catch (e) {
            console.error('[UniCMS] Cannot get data for component ' + this.name + '. ' + e);
            return {};
        }
    },
    getComponentType: function () {
        try {
            return this.component.getType();
        } catch (e) {
            console.warn('[UniCMS] Cannot get component ' + this.name + ' type. ' + e);
            return 'text';
        }
    },
    inEditMode: function () {
        var data, doc;
        try {
            if (typeof this.editMode !== 'undefined') {
                return this.editMode;
            }
            data = Template.parentData();
            if (typeof data.editMode !== 'undefined') {
                return data.editMode;
            }
            doc = this.doc || data.doc;
            check(doc, UniCollection.UniDoc);
            return doc.editMode();
        } catch (e) {
            console.error('[UniCMS] Cannot get editMode for component ' + this.name + '. ' + e);
            return false;
        }
    }
});
