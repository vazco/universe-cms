'use strict';

Template.uniEntity.helpers({
    setEntity: function () {
        try {
            if (!this.entity) {
                if (this.doc) {
                    this.entity = this.doc.getEntity();
                } else {
                    this.entity = UniCMS.entity();
                }
            }
            check(this.entity, UniCMS.Entity);
        } catch (e) {
            console.error('[UniCMS] Cannot find entity on uniEntity. ' + e);
            return null;
        }
    },
    setHooks: function (type, formId) {
        try {
            check(type, String);
            check(formId, String);
            AutoForm.addHooks(formId, this.hooks || this.entity.hooks[type], true);
        } catch (e) {
            console.error('[UniCMS] Cannot set hooks. ' + e);
        }
    },
    setEditMode: function (mode) {
        this.editMode = !!mode;
    },
    getCollection: function () {
        if (!this.entity) {
            console.error('[UniCMS] Entity is missing.');
        }
        return this.entity._collection || null;
    },
    getUpdateFields: function () {
        if (!this.entity) {
            console.error('[UniCMS] Entity is missing.');
        }
        return this.entity.getUpdateFields() || null;
    },
    getInsertFields: function () {
        if (!this.entity) {
            console.error('[UniCMS] Entity is missing.');
        }
        return this.entity.getInsertFields() || null;
    },
    getUpdateFormId: function () {
        try {
            return this.entity.getUpdateFormId(this.doc._id);
        } catch (e) {
            console.error('[UniCMS] Cannot generate form id for entity. ' + e);
            return null;
        }
    },
    getInsertFormId: function () {
        try {
            return this.entity.getInsertFormId();
        } catch (e) {
            console.error('[UniCMS] Cannot generate form id for entity. ' + e);
            return null;
        }
    },
    isUpdate: function () {
        if (!_.isUndefined(this.editMode)) {
            return this.editMode;
        }
        return this.type === 'update' || (this.doc && this.doc.editMode && this.doc.editMode()) || false;
    }
});

Template.uniEntity.events({
    'click .js-toggle-mode': function () {
        try {
            this.doc.toggleEditMode();
        } catch (e) {
            console.warn('[UniCMS] Cannot turn edit mode. ' + e);
        }
    }
});

Template.uniEntityEditViewDefaultSubmit.events({
    'click .js-delete': function (e) {
        e.preventDefault();
        var doc = this.doc;
        UniUI.areYouSure($(e.target), function () {
            try {
                var link = doc.getEntity().getLink('listing') || '/';
                if(typeof Router !== 'undefined') {
                    Router.go(link);
                } else if (typeof FlowRouter !== 'undefined') {
                    FlowRouter.go(link);
                }

                doc.remove();
            } catch (e) {
                console.warn('[UniCMS] Cannot remove doc. ' + e);
            }
        });
    }
});

Template._uniEntityDefaultView.helpers({
    getSchemaFields: function () {
        var entity = this.entity || (this.doc ? this.doc.getEntity() : UniCMS.entity());
        var schema = entity.getSchema();
        var omitFields = [];
        //gets omitting fields
        _(schema).each(function (field, name) {
            if (UniUtils.get(field, 'autoform.omit')) {
                omitFields.push(name);
            }
        });
        return _.chain(entity.getSchema()).map(function (field, name) {
            var omit = _.some(omitFields, function (v) {
                return name.indexOf(v) === 0;
            });
            if (!omit) {
                return {
                    name: name,
                    field: field,
                    component: entity.getComponent(name)
                };
            }
        }).compact().value();
    }
});

Template.uniEntityEditViewDefaultSubmit.helpers({
    inEditMode: function () {
        var data, doc;
        try {
            if (typeof this.editMode !== 'undefined') {
                return this.editMode;
            }
            data = Template.parentData();
            if (typeof data.editMode !== 'undefined') {
                return data.editMode;
            }
            doc = this.doc || data.doc;
            check(doc, UniCollection.UniDoc);
            return doc.editMode();
        } catch (e) {
            console.error('[UniCMS] Cannot get editMode for submit template "uniEntityEditViewDefaultSubmit" ' + e);
            return false;
        }
    }
});
