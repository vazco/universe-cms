'use strict';

Template.UniCMSEntityView.created = function () {
    var entity = UniCMS.entity();
    if (!entity) {
        console.warn('[UniCMS] Cannot find entity from context');
        return;
    }
    this.autorun(function () {
        var id;
        if(typeof Router !== 'undefined') {
            id = UniUtils.get(Router.current(), 'params._id');
        } else if (typeof FlowRouter !== 'undefined') {
            id = UniUtils.get(FlowRouter.current(), 'params._id');
        }

        entity.subscribe({_id: id}, {limit: 1});
    });
};

Template.UniCMSEntityView.helpers({
    item: function () {
        var id = UniUtils.get(Router.current(), 'params._id');
        var entity = UniCMS.entity();
        if (entity) {
            return entity.findOne(id);
        }
    }
});
