'use strict';

Template.UniCMSEntityListing.created = function () {
    var entity = UniCMS.entity();
    if (!entity) {
        console.warn('Controller does not have entity property on it');
        return;
    }
    this.autorun(function () {
        // remember that there are defaults on subscribe options per entity
        entity.subscribe();
    });
};

Template.UniCMSEntityListing.helpers({
    items: function () {
        var entity = UniCMS.entity();
        if (entity) {
            // remember that there are defaults on find options per entity
            return entity.find();
        }
    }
});
