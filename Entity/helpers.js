'use strict';

/**
 * Getter for entity by name or when called without params tries to find out entity from context
 * @param {String} [name] entity name
 * @returns {UniCMS.Entity|null} entity or null if not found
 */
UniCMS.entity = function (name) {
    var entity;
    if (name || Meteor.isServer) {
        return UniCMS._entities[name] || null;
    }
    var router;
    try {
        if(typeof Router !== 'undefined') {
            router = Router.current();
        } else if (typeof FlowRouter !== 'undefined') {
            router = FlowRouter.current();
        }

        if (typeof Router !== 'undefined') {
            entity = router.entity || router.data().entity;
        }
        entity = entity || this.entity;
        check(entity, UniCMS.Entity);
        return entity;
    } catch (e) {
        console.warn('[UniCMS] Cannot find entity. ' + e);
        return null;
    }
};

if (Meteor.isClient) {
    Template.registerHelper('UniCMSEntity', UniCMS._entities);
    Template.registerHelper('getEntity', UniCMS.entity);
}
