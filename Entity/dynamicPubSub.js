'use strict';

/**
 * Set up entity dynamic publication
 * @returns {UniCMS.Entity} entity
 * @private
 */
UniCMS.Entity.prototype._initDynamicPubSub = function () {
    if (!Meteor.isServer) {
        return this;
    }

    var pubName = 'UniCMS/' + this.name;

    var collection = this._collection;
    //Allows to publish only for our dynamic publication, other publication it's not our matter
    collection.allow({
        publish: function(userId, doc, publicationName){
            return publicationName === pubName
        }
    });

    UniCollection.publish(pubName, function (selector, options) {
        // @todo maybe some validation?
        return collection.find(selector || {}, options || {});
    });
    return this;
};

/**
 * Subscribe for entity data using UniCMS dynamicPublish function
 * You should pass the same parameters you do to find function
 * There are some defaults set on options, @see UniCMS.Entity._subscribeOptions
 * @param {Object} [selector={}] mongo query selector
 * @param {Object} [options={}] mongo query options
 * @param {Function} subscribeFunction it's subscription getter (must be bind to own context!)
 * example: entity.subscribe({},{}, templateInstance.subscribe.bind(templateInstance));
 * @returns {Object} Subscription handler
 */
UniCMS.Entity.prototype.subscribe = function (selector, options, subscribeFunction) {
    if (!UniUtils.get(this, 'options.dynamicPubSub')) {
        throw new Meteor.Error(403, 'dynamicPubSub is disabled for ' + this.name, ', you cannot subscribe to it!');
    }
    subscribeFunction = _.isFunction(subscribeFunction) ? subscribeFunction : Meteor.subscribe.bind(Meteor);
    return subscribeFunction('UniCMS/' + this.name,
        _(selector || {}).defaults(this._subSelector),
        _({}).extend(this._subOptions, options)
    );
};

/**
 * Object with selector defaults for UniCMS.Entity.subscribe
 * @type {Object}
 */
UniCMS.Entity.prototype._subSelector = {};

/**
 * Object with options preselected for UniCMS.Entity.subscribe
 * @type {Object}
 */
UniCMS.Entity.prototype._subOptions = {
    limit: 10,
    sort: {
        createdAt: -1
    }
};
