'use strict';

/**
 * Sets up routes and some other stuff required for entities to work out-of-the-box
 * @param {Object} cfg - dynamicRouter configuration object
 * @returns {UniCMS.Entity} entity
 * @private
 */
UniCMS.Entity.prototype._initDynamicRouter = function (cfg) {
    if (!Meteor.isClient) {
        return this;
    }
    if (!Iron) {
        throw new Meteor.Error(500, 'Iron:Router is required for dynamicRouter');
    }
    check(cfg, Object);

    var entity = this;

    // Set default values

    _(cfg).defaults({
        slug: entity.name
    });


    // Create controller for entity, this can be configured in few ways
    // We're usually adding here entity property for controller used later
    // to find entity context in templates

    if (cfg.controller instanceof Iron.Controller.constructor) {
        // controller object as a param - pass it without changes
        // this will not add entity property to controller!
        entity.controller = cfg.controller;
    } else if (_.isObject(cfg.controller)) {
        // object config as a param - extend controller
        cfg.controller.entity = cfg.controller.entity || entity;
        entity.controller = UniCMS.Controller.extend(cfg.controller);
    } else if (cfg.controller === false) {
        // controller is disabled, not sure if needed but leaving this as an option
        entity.controller = null;
    } else {
        // default, use global controller with entity reference
        entity.controller = UniCMS.Controller.extend({
            entity: entity
        });
    }

    check(entity.controller, Match.OneOf(Iron.Controller.constructor, null));

    // Create routes from passed dynroutes or global UniCMS dynroutes setting

    _(cfg.dynroutes || UniCMS._dynroutes).each(function (dynroute, name) {
        // config can be overwritten per route
        var options = _({}).extend(cfg, cfg[name] || {});
        var route = options.route = options.route || {};

        // if one don't specify route params in config we create them from dynroute

        _(route).defaults({
            name: dynroute.name(entity, options),
            path: dynroute.path(entity, options),
            template: dynroute.template(entity, options)
        });

        if (entity.controller) {
            if (cfg[name] && cfg[name].controller) {
                // Controller can be customized per route if needed

                if (cfg[name] && cfg[name].controller instanceof Iron.Controller.constructor) {
                    // we got controller just for this route
                    route.controller = cfg[name].controller;
                } else if (_.isObject(cfg[name].controller)) {
                    // we got customized controller options for a route, so create a new controller
                    route.controller = entity.controller.extend(cfg[name].controller);
                }
            } else {
                // no special options so just use global entity controller
                route.controller = entity.controller;
            }

            check(route.controller, Iron.Controller.constructor);
        }

        //console.log('route', options);

        // Create new route

        if(typeof Router !== 'undefined') {
            Router.route(route.path, route);
        } else if (typeof FlowRouter !== 'undefined') {
            FlowRouter.route(route.path, route);
        }
    });

    return this;
};


/**
 * Default settings for dynamicRouter
 * @type {{listing: {name: Function, path: Function, template: Function}, add: {name: Function, path: Function, template: Function}, view: {name: Function, path: Function, template: Function}}}
 * @private
 */
UniCMS._dynroutes = {
    listing: {
        name: function (entity) {
            return entity.name + 'Listing';
        },
        path: function (entity, options) {
            return '/' + options.slug || entity.name;
        },
        template: function (entity) {
            var tmplName = entity.name + 'Listing';
            return Template[tmplName] ? tmplName : 'UniCMSEntityListing';
        }
    },
    add: {
        name: function (entity) {
            return entity.name + 'Add';
        },
        path: function (entity, options) {
            return '/' + (options.slug || entity.name) + '/add';
        },
        template: function (entity) {
            var tmplName = entity.name + 'Add';
            return Template[tmplName] ? tmplName : 'UniCMSEntityAdd';
        }
    },
    view: { // all fixed routes must precedence view because of wildcard!
        name: function (entity) {
            return entity.name + 'View';
        },
        path: function (entity, options) {
            return '/' + (options.slug || entity.name) + '/:_id';
        },
        template: function (entity) {
            var tmplName = entity.name + 'View';
            return Template[tmplName] ? tmplName : 'UniCMSEntityView';
        }
    }
};
