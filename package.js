'use strict';

Package.describe({
    name: 'vazco:universe-cms',
    version: '0.5.8',
    summary: 'Content Management System for Universe Framework',
    git: 'https://bitbucket.org/vazco/universe-cms',
    documentation: 'README.md'
});

Package.onUse(function (api) {
    api.versionsFrom('1.2.0.2');

    api.use([
        'check',
        'underscore'
    ]);

    api.use([
        'templating',
        'blaze',
        'reactive-dict'
    ], ['client']);

    // api.use([
    //     'iron:router@1.0.0'
    // ], ['client', 'server'], {weak: true});

    api.use([
        'universe:utilities@2.0.9',
        'universe:collection@2.0.4',
        'vazco:universe-ui@0.6.5',
        'aldeed:autoform@5.0.0||4.2.2'
    ]);

    if(!api.addAssets) {
        api.addAssets = function(files, platform){
            api.addFiles(files, platform, {isAsset: true})
        };
    }

    // Main

    api.addFiles([
        'UniCMS.js'
    ]);

    // Component

    api.addFiles([
        'Component/UniCMS.Component.js',
        'Component/helpers.js'
    ]);

    api.addFiles([
        'Component/basicComponents/metadata.html',
        'Component/basicComponents/text.html',
        'Component/basicComponents/textarea.html'
    ], 'client');

    api.addFiles([
        'Component/basicComponents/metadata.js',
        'Component/basicComponents/text.js',
        'Component/basicComponents/textarea.js'
    ]);

    // Entity

    api.addFiles([
        'Entity/UniCMS.Controller.js',
        'Entity/UniCMS.EntityDoc.js',
        'Entity/UniCMS.Entity.js',
        'Entity/dynamicPubSub.js',
        'Entity/dynamicRouter.js',
        'Entity/helpers.js'
    ]);

    api.addFiles([
        'Entity/helpers/uniComponent.html',
        'Entity/helpers/uniEntity.html',
        'Entity/helpers/uniLink.html'
    ], 'client');

    api.addFiles([
        'Entity/helpers/uniComponent.js',
        'Entity/helpers/uniEntity.js',
        'Entity/helpers/uniLink.js'
    ], 'client');

    api.addFiles([
        'Entity/defaultViews/UniCMSEntityAdd.html',
        'Entity/defaultViews/UniCMSEntityListing.html',
        'Entity/defaultViews/UniCMSEntityView.html'
    ], 'client');

    api.addFiles([
        'Entity/defaultViews/UniCMSEntityAdd.js',
        'Entity/defaultViews/UniCMSEntityListing.js',
        'Entity/defaultViews/UniCMSEntityView.js'
    ], 'client');

    api.export('UniCMS');
});

Package.onTest(function (api) {
    api.use('tinytest');
    api.addFiles('tests/tests.js');
});
