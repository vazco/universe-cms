'use strict';

/**
 * Creates new basic component type and register new AutoForm input type.
 * @param {String} type - Type of the component.
 * You can use pre-existing AutoForm component as a type or create new one with addInputType option.
 * @param {Object} options - Component configuration
 * @throws {Meteor.Error}
 * @constructor
 */
UniCMS.Component = function (type, options) {
    var component = this;

    // Verify component arguments
    check(type, String);
    check(options, Object);

    if (!(component instanceof UniCMS.Component)) {
        throw new Meteor.Error(500, 'UniCMS.Component is constructor used to create new component types.' +
        'Use UniCMS.component to extend or make use of component');
    }

    // Set type
    if (!type) {
        throw new Meteor.Error(500, 'UniCMS component require a type');
    }
    component.setType(type);

    // Set component extrinsic default values
    _(component).defaults({
        name: options.name || type
    });

    // Create an AutoForm custom input type
    if (options.addInputType) {
        if (Meteor.isClient) {
            if (Match.test(options.addInputType, {
                    template: String,
                    valueIn: Match.Optional(Function),
                    valueOut: Function,
                    valueConverters: Match.Optional(Object),
                    contextAdjust: Match.Optional(Function)
                })) {
                AutoForm.addInputType(component._type, options.addInputType);
            } else {
                console.error('[UniCMS] Invalid addInputType while adding component ' + component.name);
            }
        }
        options.addInputType = null; //we don't need this option anymore
    }


    // Set component options
    component.extend(options);
};

/**
 * Deep extend of object with prototype inheritance
 * @param {Object} target - object to copy to
 * @param {Object} source - object to copy from
 * @private
 * @returns {Object|*} extended object
 */
function proxyExtend (target, source) {
    var prop;
    for (prop in source) {
        if (prop in target &&
            typeof source[prop] === 'object' &&
            typeof target[prop] === 'object' &&
            source[prop].constructor !== Array &&
            target[prop].constructor !== Array) {
            // Object collision, we need to go deeper!
            target[prop] = proxyExtend(Object.create(target[prop]), source[prop]);
        } else {
            // Normal extending. Possible support for other types in the future.
            target[prop] = source[prop];
        }
    }
    return target;
}

/**
 * Smart way of deep extending component options.
 * @param {Object} source - Properties to extend from
 * @returns {UniCMS.Component} extended component
 */
UniCMS.Component.prototype.extend = function (source) {
    return proxyExtend(this, source);
};

/**
 * Getter for component type
 * @returns {string} component autoform input type
 */
UniCMS.Component.prototype.getType = function () {
    return this._type;
};

/**
 * Setter for component type. This need to be AutoForm registered input type!
 * @param {string} newType - autoform input type
 * @returns {UniCMS.Component} component
 */
UniCMS.Component.prototype.setType = function (newType) {
    check(newType, String);
    // As of now there is no way to check if given type is registered inside AutoForm.
    // I hope that this will change in the future so we can perform additional validation
    // over here instead of template throwing an error.
    this._type = newType;
    return this;
};

UniCMS.Component.prototype.schema = {
    type: String
};

UniCMS.Component.prototype.getSchema = function () {
    return this.schema;
};
