'use strict';

var component = new UniCMS.Component('hidden', {
    template: 'UniCMSComponentMetadataOutput',
    schema: {
        type: String
    }
});

UniCMS.registerComponent('metadata', component);
