'use strict';

var component = new UniCMS.Component('textarea', {
    template: 'UniCMSComponentTextareaOutput',
    schema: {
        type: String,
        autoform: {
            afFieldInput: {
                style: 'height: 100px;'
            }
        }
    }
});

UniCMS.registerComponent('textarea', component);

if (Meteor.isClient) {
    Template.UniCMSComponentTextareaOutput.helpers({
        splitLines: function (str) {
            if (!str) {
                return null;
            }
            check(str, String);
            return str.split(/\r\n|\n\r|\n|\r/);
        }
    });
}
