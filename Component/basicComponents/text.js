'use strict';

var component = new UniCMS.Component('text', {
    template: 'UniCMSComponentTextOutput',
    schema: {
        type: String
    }
});

UniCMS.registerComponent('text', component);
