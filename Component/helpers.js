'use strict';

/**
 * Register new component that can be used in UniCMS
 * @param {String} name - Name of the component
 * @param {UniCMS.Component} component - component
 * @returns {undefined} nothing
 */
UniCMS.registerComponent = function (name, component) {
    check(name, String);
    check(component, this.Component);

    if (this._components[name]) {
        console.log('[UniCMS] Component ' + name + ' will be overwritten');
    }
    this._components[name] = component;
};

/**
 * Creates new component instance
 * @param {String} name - Name of the component to use
 * @param {Object} [options] - Component customizations and settings
 * @throws {Meteor.Error}
 * @returns {UniCMS.Component} component
 */
UniCMS.component = function (name, options) {
    check(name, String);

    if (this !== UniCMS) {
        throw new Meteor.Error(500, 'UniCMS.component is not a constructor!');
    }

    var component = this._components[name];

    if (!(component instanceof this.Component)) {
        throw new Meteor.Error(500, 'There is no registered component named ' + name);
    }

    options = options || {};
    options.name = options.name || name;

    return (Object.create(component)).extend(options);
};

/**
 * Searches for component in parent templates scopes.
 * This is intended to use within components' template helpers.
 * @return {UniCMS.Component|null} component or null if not found
 */
UniCMS.findComponent = function () {
    var lvl = 1, data;
    try {
        data = Blaze.getData() || {};
        while (data && !data.component) {
            data = Template.parentData(lvl++);
        }
        if (!data || !data.component) {
            throw 'No component in scope.';
        }
        check(data.component, UniCMS.Component);
        return data.component;
    } catch (e) {
        console.error('[UniCMS] Cannot find component. ' + e);
        return null;
    }
};

UniCMS.findComponentContentBlock = function () {
    var lvl = 1, data;
    try {
        data = Blaze.getData() || {};
        while (data && !data.componentContentBlock) {
            data = Template.parentData(lvl++);
        }
        if (!data || !data.componentContentBlock) {
            return null;
        }
        check(data.componentContentBlock, Blaze.Template);
        return data.componentContentBlock;
    } catch (e) {
        return null;
    }
};
UniCMS.findComponentElseBlock = function () {
    var lvl = 1, data;
    try {
        data = Blaze.getData() || {};
        while (data && !data.componentElseBlock) {
            data = Template.parentData(lvl++);
        }
        if (!data || !data.componentElseBlock) {
            return null;
        }
        check(data.componentElseBlock, Blaze.Template);
        return data.componentElseBlock;
    } catch (e) {
        return null;
    }
};

if (Meteor.isClient) {
    Template.registerHelper('componentContentBlock', UniCMS.findComponentContentBlock);
    Template.registerHelper('componentElseBlock', UniCMS.findComponentElseBlock);
}
